#ifndef ADDDIALOG_H
#define ADDDIALOG_H
#include <QDialog>
#include <QVBoxLayout>
#include <QLineEdit>
#include <QLabel>
#include <QPushButton>


class AddDialog : public QDialog
{
    Q_OBJECT
private:
    QWidget *fwid;
    QWidget *twid;
    QVBoxLayout *flayout;
    QVBoxLayout *tlayout;
    //QVBoxLayout *slayout;
    QPushButton *ok;
public:
    AddDialog(QWidget *parent = 0);
    ~AddDialog();
public slots:
    void cr_dial(int);

};

#endif // ADDDIALOG_H
