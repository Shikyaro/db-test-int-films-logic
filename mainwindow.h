#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QMessageBox>
#include <QMenuBar>
#include <QComboBox>
#include <QTableWidget>
#include <QTabWidget>
#include <QMenu>
#include <QStatusBar>
#include <QDateTime>
#include "film.h"
#include "filmwidget.h"


class MainWindow : public QMainWindow
{
    Q_OBJECT
private:
    QHBoxLayout *layout;

    QTableWidget *table;
    QPushButton *add;
    QPushButton *search;
    QPushButton *tofile;
    QPushButton *fromfile;

    QLabel *qla;
public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
private slots:
    void aut_info();
    void prog_info();
public slots:
signals:


};

#endif // MAINWINDOW_H
