#include "filmwidget.h"

FilmWidget::FilmWidget(QWidget *parent) : QWidget(parent)
{
    films = new film_list;
    QHBoxLayout *mlay = new QHBoxLayout;
    this->setLayout(mlay);
    QVBoxLayout *lay2 = new QVBoxLayout;
    mlay->addLayout(lay2);
    add = new QPushButton("Add");
    lay2->addWidget(add);
    search = new QPushButton("Search");
    lay2->addWidget(search);
    del = new QPushButton("Delete");
    lay2->addWidget(del);
    trunc = new QPushButton("Truncate");
    lay2->addWidget(trunc);
    tofile = new QPushButton("Load to file");
    lay2->addWidget(tofile);
    fromfile = new QPushButton("Load from file");
    lay2->addWidget(fromfile);

    table = new QTableWidget;
    mlay->addWidget(table);
    table->setColumnCount(3);

    //table->setCellWidget(1,1,new QPushButton(0));
    QStringList names;
    names << "Name" << "Length" << "Country";
    table->setHorizontalHeaderLabels(names);
    table->setColumnWidth(1,50);

    connect(add,SIGNAL(clicked()),this,SLOT(add_r()));
    connect(addok,SIGNAL(clicked()),this,SLOT(send_add()));
    connect(this,SIGNAL(adder(QString,int,QString)),films,SLOT(add_test(QString,int,QString)));

    connect(del,SIGNAL(clicked()),this,SLOT(del_r()));
    connect(del_ok,SIGNAL(clicked()),this,SLOT(send_del()));
    connect(this,SIGNAL(deleter(int)),films,SLOT(del_id(int)));

    connect(trunc,SIGNAL(clicked()),this,SLOT(trun()));
    connect(this,SIGNAL(truncate()),films,SLOT(trunc_test()));

    connect(search,SIGNAL(clicked()),this,SLOT(search_r()));
    connect(search_ok,SIGNAL(clicked()),this,SLOT(send_search()));
    connect(this,SIGNAL(searcher(QString)),films,SLOT(search_test(QString)));

    connect(tofile,SIGNAL(clicked()),this,SLOT(to_file()));
    connect(to_ok,SIGNAL(clicked()),this,SLOT(send_to()));
    connect(this,SIGNAL(to_filer(QString)),films,SLOT(to_test(QString)));

    connect(fromfile,SIGNAL(clicked()),this,SLOT(from_file()));
    connect(from_ok,SIGNAL(clicked()),this,SLOT(send_from()));
    connect(this,SIGNAL(from_filer(QString)),films,SLOT(from_test(QString)));

    //connect(table,SIGNAL(cellPressed(int,int)),this,SLOT(aciv_test(int,int)));
    connect(table,SIGNAL(cellChanged(int,int)),this,SLOT(change_cell(int,int)));
    connect(this,SIGNAL(ch_ce(int,int,QString)),films,SLOT(changed_cell(int,int,QString)));

    connect(films,SIGNAL(add_row(QString,int,QString)),this,SLOT(add_row(QString,int,QString)));

    connect(films,SIGNAL(snd_search(film*)),this,SLOT(sh_ser_rec(film*)));

    connect(ser_ok,SIGNAL(clicked()),this,SLOT(rec_changer()));
    connect(ser_del,SIGNAL(clicked()),this,SLOT(sh_dl()));

}

void FilmWidget::trun()
{
    emit this->truncate();
    //table->clear();
    table->setRowCount(0);
}
 void FilmWidget::change_cell(int ro, int co)
{
    QTableWidgetItem *tmp = table->item(ro, co);
    emit this->ch_ce(ro,co, tmp->text());
}

FilmWidget::~FilmWidget()
{

}
void FilmWidget::add_r()
{
    addw = new QWidget;
    addw->setWindowTitle("Добавление нового фильма");
    QGridLayout *mgr = new QGridLayout;
    mgr->addWidget(new QLabel("Название:"),0,0);
    name = new QLineEdit;
    mgr->addWidget(name,0,1);
    mgr->addWidget(new QLabel("Длина:"),1,0);
    length = new QLineEdit;
    mgr->addWidget(length,1,1);
    mgr->addWidget(new QLabel("Страна:"),2,0);
    country = new QLineEdit;
    mgr->addWidget(country,2,1);
    mgr->addWidget(addok,3,1);
    addw->setLayout(mgr);
    addw->show();
    connect(addok,SIGNAL(clicked()),addw,SLOT(close()));

}
void FilmWidget::send_add()
{
    QString nm = name->text();
    QString len = length->text();
    QString cou = country->text();
    emit this->adder(tr("%1").arg(name->text()),length->text().toInt(),tr("%1").arg(country->text()));
    table->insertRow(table->rowCount());
    table->setItem(table->rowCount()-1,0,new QTableWidgetItem(tr("%1").arg(nm)));
    table->setItem(table->rowCount()-1,1,new QTableWidgetItem(tr("%1").arg(len)));
    table->setItem(table->rowCount()-1,2,new QTableWidgetItem(tr("%1").arg(cou)));
    //emit this->adder(name->text(),length->text().toInt(),country->text());
}
void FilmWidget::search_r()
{
    searchw = new QWidget;
    searchw->setWindowTitle("Поиск фильма по названию");
    QGridLayout *mgr = new QGridLayout;
    mgr->addWidget(new QLabel("Название:"),0,0);
    search_str = new QLineEdit;
    mgr->addWidget(search_str,0,1);
    mgr->addWidget(search_ok,1,1);
    mgr->addWidget(search_cancel,1,0);
    searchw->setLayout(mgr);
    searchw->show();
    connect(search_ok,SIGNAL(clicked()),searchw,SLOT(close()));
    connect(search_cancel,SIGNAL(clicked()),searchw,SLOT(close()));
}
void FilmWidget::send_search()
{
    emit this->searcher(search_str->text());
    delete search_str;
}
void FilmWidget::del_r()
{
    delw = new QWidget;
    delw->setWindowTitle("Удаление фильма по id");
    QGridLayout *mgr = new QGridLayout;
    mgr->addWidget(new QLabel("Id:"),0,0);
    delid = new QLineEdit;
    mgr->addWidget(delid,0,1);
    mgr->addWidget(del_ok,1,1);
    QPushButton *del_can = new QPushButton("Cancel");
    mgr->addWidget(del_can,1,0);
    delw->setLayout(mgr);
    delw->show();
    connect(del_ok,SIGNAL(clicked()),delw,SLOT(close()));
    connect(del_can,SIGNAL(clicked()),delw,SLOT(close()));
}
void FilmWidget::send_del()
{
        emit this->deleter(delid->text().toInt());
        table->removeRow(delid->text().toInt()-1);
}

void FilmWidget::to_file()
{
    loadto = new QWidget;
    loadto->setWindowTitle("Вывод в файл");
    QGridLayout *mgr = new QGridLayout;
    mgr->addWidget(new QLabel("Путь к файлу:"),0,0);
    to_path = new QLineEdit;
    mgr->addWidget(to_path,0,1);
    mgr->addWidget(to_ok,1,1);
    mgr->addWidget(to_cancel,1,0);
    loadto->setLayout(mgr);
    loadto->show();
    connect(to_ok,SIGNAL(clicked()),loadto,SLOT(close()));
    connect(to_cancel,SIGNAL(clicked()),loadto,SLOT(close()));

}
void FilmWidget::send_to()
{
    emit this->to_filer(to_path->text());
    delete to_path;
}
void FilmWidget::from_file()
{
    loadfrom = new QWidget;
    loadfrom->setWindowTitle("Ввод из файла");
    QGridLayout *mgr = new QGridLayout;
    mgr->addWidget(new QLabel("Путь к файлу:"),0,0);
    from_path= new QLineEdit;
    mgr->addWidget(from_path,0,1);
    mgr->addWidget(from_ok,1,1);
    QPushButton *from_can = new QPushButton("Отмена");
    mgr->addWidget(from_can,1,0);
    loadfrom->setLayout(mgr);
    loadfrom->show();
    connect(from_ok,SIGNAL(clicked()),loadfrom,SLOT(close()));
    connect(from_can,SIGNAL(clicked()),loadfrom,SLOT(close()));
}
void FilmWidget::send_from()
{
    emit this->from_filer(from_path->text());
}
void FilmWidget::add_row(QString nm, int le , QString cou)
{
    table->insertRow(table->rowCount());
    table->setItem(table->rowCount()-1,0,new QTableWidgetItem(tr("%1").arg(nm)));
    table->setItem(table->rowCount()-1,1,new QTableWidgetItem(tr("%1").arg(le)));
    table->setItem(table->rowCount()-1,2,new QTableWidgetItem(tr("%1").arg(cou)));
}
void FilmWidget::sh_ser_rec(film* rec)
{
    reco = rec;
    show_s_rec = new QWidget;
    QGridLayout *mgr = new QGridLayout;
    mgr->addWidget(new QLabel("Название"),0,0);
    mgr->addWidget(new QLabel("Длина"),1,0);
    mgr->addWidget(new QLabel("Страна"),2,0);
    nme = new QLineEdit;
    nme->setText(reco->get_name());
    mgr->addWidget(nme,0,1);
    leng = new QLineEdit;
    leng->setText(tr("%1").arg(reco->get_length()));
    mgr->addWidget(leng,1,1);
    cou = new QLineEdit;
    cou->setText(reco->get_country());
    mgr->addWidget(cou,2,1);
    QHBoxLayout *butlay = new QHBoxLayout;
    butlay->addWidget(ser_del);
    butlay->addWidget(ser_ok);
    mgr->addLayout(butlay,3,1);
    show_s_rec->setLayout(mgr);
    show_s_rec->show();

    connect(ser_del,SIGNAL(clicked()),show_s_rec,SLOT(close()));
    connect(ser_ok,SIGNAL(clicked()),show_s_rec,SLOT(close()));

    //connect(ser_del)
}
void FilmWidget::rec_changer()
{
    reco->set_name(nme->text());
    reco->set_length(leng->text().toInt());
    reco->set_country(cou->text());
    QTableWidgetItem *nm = new QTableWidgetItem();
    nm->setText(nme->text());
    table->setItem(reco->get_id()-1,0,nm);
    QTableWidgetItem *le = new QTableWidgetItem();
    le->setText(leng->text());
    table->setItem(reco->get_id()-1,1,le);
    QTableWidgetItem *co = new QTableWidgetItem();
    co->setText(cou->text());
    table->setItem(reco->get_id()-1,2,co);
}
void FilmWidget::sh_dl()
{
    emit this->deleter(reco->get_id());
    table->removeRow(reco->get_id()-1);
}
