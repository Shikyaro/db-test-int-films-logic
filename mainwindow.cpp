#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    QMenu *file = menuBar()->addMenu("File");
    QMenu *about = menuBar()->addMenu("About");

    QAction *quit = new QAction("Выход",file);
    QAction *author = new QAction("Об авторе",about);
    QAction *program = new QAction("О программе",about);
    file->addAction(quit);
    about->addAction(author);
    about->addAction(program);
    this->setWindowTitle("БД");
    QWidget *mwid = new QWidget;
    layout = new QHBoxLayout;
    QVBoxLayout *llay = new QVBoxLayout;
    mwid->setLayout(layout);
    this->setCentralWidget(mwid);
    FilmWidget *ftab = new FilmWidget;
    QTabWidget *tbw = new QTabWidget;
    tbw->addTab(ftab,"Films");
    tbw->addTab(new QLabel("В разработке"),"Theaters");
    tbw->addTab(new QLabel("В разработке"), "Sessions");
    layout->addWidget(tbw);
    qla = new QLabel("123");
    //layout->addWidget(qla);
    //table->hide();
    connect(quit,SIGNAL(triggered()),this,SLOT(close()));
    connect(author,SIGNAL(triggered()),this,SLOT(aut_info()));
    connect(program,SIGNAL(triggered()),this,SLOT(prog_info()));
    //connect(program,SIGNAL(triggered()),table,SLOT(hide()));


}

MainWindow::~MainWindow()
{

}
void MainWindow::aut_info()
{
    QMessageBox::information(this,"Об авторе","Автор: Романов Павел (Alkor)\nГруппа: П-204");
    layout->addWidget(qla);
    qla->show();
}
void MainWindow::prog_info()
{
    QMessageBox::information(this,"О программе","Тестовая версия таблицы\nс фильмами (задание 2-17)");
    layout->removeWidget(qla);
    qla->hide();

}
