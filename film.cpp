#include "film.h"

film::film(QObject *parent) : record(parent)
{

}

film::~film()
{

}

void film::set_name(QString nname)
{
    name = nname;
}

void film::set_length(int nlength)
{
    length = nlength;
}

void film::set_country(QString ncountry)
{
    country = ncountry;
}
void film::set_next(film* nnext)
{
    next = nnext;
}
void film::set_prev(film* nprev)
{
    prev = nprev;
}


QString film::get_name()
{
    return name;
}

int film::get_length()
{
    return length;
}

QString film::get_country()
{
    return country;
}
film* film::get_next()
{
    return next;
}
film* film::get_prev()
{
    return prev;
}

film_list::film_list(QObject *parent) : QObject(parent)
{

}
void film_list::add(QString nname, unsigned int nlen, QString ncou)
{
    film *tmp = new film;
    tmp->set_name(nname);
    tmp->set_length(nlen);
    tmp->set_country(ncou);
    tmp->set_next(NULL);
    if (first != NULL){
        tmp->set_prev(last);
        last->set_next(tmp);
        last = tmp;
        last->set_id(last->get_prev()->get_id() + 1);
    }
    else{
        //tmp->set_prev(NULL);
        tmp->set_id(1);
        first = tmp;
        first->set_prev(NULL);
        first->set_next(last);
        last = tmp;
        last->set_prev(first);
        last->set_next(NULL);
    }
}
film *film_list::search(QString sstr)
{
    film *temp = first;
    while (temp){
        if (temp->get_name() == sstr){
            return temp;
        }
        temp = temp->get_next();
    }
    return NULL;
}
film* film_list::search_id(int sid)
{
    film *temp = first;
    while (temp){
        if (temp->get_id() == sid){
            return temp;
        }
        temp = temp->get_next();
    }
    return NULL;
}
/*void film_list::del(film *dfil)
{
    film* temp;
        //slist->del_by_name(dfil->get_film_name());
        if (dfil == first){
            first = first->get_next();
            if (first){
                first->set_prev(NULL);
                temp = first;
                while (temp != NULL){
                    temp->set_id(temp->get_id() - 1);
                    //QMessageBox::information(0,"0",tr("%1").arg(temp->get_id()));
                    temp = temp->get_next();
                }
            }
            delete dfil;
        }
        else if (dfil == last){
            last = last->get_prev();
            if (last){
                last->set_next(NULL);
            }
            delete dfil;
        }
        else{
            dfil->get_prev()->set_next(dfil->get_next());
            dfil->get_next()->set_prev(dfil->get_prev());
            temp = dfil;
            while (temp != NULL){
                temp->set_id(temp->get_id() - 1);
                temp = temp->get_next();
            }
            delete dfil;
        }
}*/
void film_list::del(film *ftd){
    //this->del_sson(ftd->get_film_name());
    film* temp;
    //s_list->del_by_name(ftd->get_film_name());
    if (ftd == first){
        first = first->get_next();
        if (first){
            first->set_prev(NULL);
            temp = first;
            while (temp->get_next() != NULL){
                temp->set_id(temp->get_id() - 1);
                temp = temp->get_next();
            }
        //delete ftd;
    }else if (ftd == last){
        last = last->get_prev();
        if (last){
            last->set_next(NULL);
        }
        //delete ftd;
    }else{
        ftd->get_prev()->set_next(ftd->get_next());
        ftd->get_next()->set_prev(ftd->get_prev());
        temp = ftd;
        while (temp->get_next() != NULL){
            temp->set_id(temp->get_id() - 1);
            temp = temp->get_next();
        }
       //delete ftd;
    }
}}

void film_list::trunc()
{
    film* temp = first;
    while (temp){
        film* temp2 = temp;
        temp = temp->get_next();
        del(temp2);
    }
}
void film_list::to_file(QString path)
{
    QFile outf(path);
    outf.open(QFile::WriteOnly);
    film* temp = first;
    QTextStream out(&outf);
    while(temp)
    {
        out << temp->get_name() << " " << tr("%1").arg(temp->get_length()) << " " << temp->get_country();
        temp = temp->get_next();
    }
    outf.close();

}

void film_list::add_test(QString nname, int nlen, QString ncou)
{
    this->add(nname,nlen,ncou);
    QMessageBox *msg = new QMessageBox;
    msg->setText(tr("Будет добавлено:\nНазвание: %1 Длина: %2 Страна: %3").arg(nname).arg(nlen).arg(ncou));
    msg->show();
}
void film_list::trunc_test()
{
    this->trunc();
    QMessageBox *msg = new QMessageBox;
    msg->setText("Таблица film_list очищена");
    msg->show();
}
void film_list::search_test(QString sname)
{
    //QMessageBox *msg = new QMessageBox;
    //msg->setText(tr("Будет найден элемент с именем %1").arg(sname));
    //msg->show();
    film* temp;
    temp = this->search(sname);
    emit this->snd_search(temp);

}
void film_list::to_test(QString tpath)
{
    this->to_file(tpath);
}
void film_list::from_test(QString fpath)
{
    QMessageBox::information(0,"Ввод из файла",tr("Таблица будет загружена из файла\n%1").arg(fpath));
    QFile inf(fpath);
    inf.open(QFile::ReadOnly);
    while (!inf.atEnd())
    {
        QString str = inf.readLine();
        str.trimmed();
        QStringList lst = str.split(" ");
        this->add(tr("%1").arg(lst[0]),(tr("%1").arg(lst[1])).toInt(),tr("%1").arg(lst[2]));
        emit this->add_row(tr("%1").arg(lst[0]),(tr("%1").arg(lst[1])).toInt(),tr("%1").arg(lst[2]));
    }
}
void film_list::del_id(int did)
{
    if (did == 1){
        //this->trunc();
    //del(search_id(did));
        del(first);
        //first = NULL;
    }else{
        del(search_id(did));
    }
    QMessageBox::information(0,"Удаление",tr("Запись удалена"));
}
void film_list::changed_cell(int ro, int co, QString tx)
{
    film *tmp = this->search_id(ro+1);
    switch (co) {
    case 0:
        tmp->set_name(tx);
        break;
    case 1:
        tmp->set_length(tx.toInt());
        break;
    case 2:
        tmp->set_country(tx);
        break;
    }
}
