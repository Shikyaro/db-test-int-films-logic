#ifndef FILM_H
#define FILM_H
#include "record.h"
#include <QString>
#include <QMessageBox>
#include <QFile>
#include <QTextStream>

class film : public record
{
private:
    film* next = NULL;
    film* prev = NULL;
    QString name;
    unsigned int length;
    QString country;
public:
    film(QObject *parent = 0);
    void set_name(QString);
    void set_length(int);
    void set_country(QString);
    void set_next(film*);
    void set_prev(film*);

    QString get_name();
    int get_length();
    QString get_country();
    film* get_next();
    film* get_prev();
    ~film();
};

class film_list : public QObject
{
    Q_OBJECT

private:
    film *first = NULL;
    film *last = NULL;
public:
    film_list(QObject *parent = 0);
    void add(QString, unsigned int, QString);
    void del(film*);
    void trunc();
    film* search_id(int);
    film* search(QString);
    void to_file(QString);
    void from_file(QString);
public slots:
    void add_test(QString, int, QString);
    void del_id(int);
    void trunc_test();
    void search_test(QString);
    void to_test(QString);
    void from_test(QString);
    void changed_cell(int, int, QString tx);
signals:
    void testmsg(QString);
    void del_sessions(QString);
    void add_row(QString, int, QString);
    void snd_search(film*);

};

#endif // FILM_H
