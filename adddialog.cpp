#include "adddialog.h"

AddDialog::AddDialog(QWidget *parent) : QDialog(parent)
{
    ok = new QPushButton;
    fwid = new QWidget();
    flayout = new QVBoxLayout;
    fwid->setLayout(flayout);
    flayout->addWidget(new QLineEdit(0));
    flayout->addWidget(new QLineEdit(0));
    flayout->addWidget(new QLineEdit(0));
    flayout->addWidget(ok);
    twid = new QWidget();
    tlayout = new QVBoxLayout;
    twid->setLayout(tlayout);
    tlayout->addWidget(new QLineEdit(0));
    tlayout->addWidget(new QLineEdit(0));
    tlayout->addWidget(new QLineEdit(0));
    tlayout->addWidget(new QLineEdit(0));
    tlayout->addWidget(ok);

    connect(ok,SIGNAL(clicked()),this,SLOT(accept()));
}

AddDialog::~AddDialog()
{

}
void AddDialog::cr_dial(int t_n)
{
    switch (t_n) {
    case 0:
    {
        fwid->show();
        this->show();
        break;
    }
    case 1:
    {
        twid->show();
        this->show();
        break;
    }
    default:
        break;
    }
}
