#-------------------------------------------------
#
# Project created by QtCreator 2015-03-09T12:55:22
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = InterfTest
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    record.cpp \
    film.cpp \
    filmwidget.cpp

HEADERS  += mainwindow.h \
    record.h \
    film.h \
    filmwidget.h
