#ifndef FILMWIDGET_H
#define FILMWIDGET_H

#include <QWidget>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QMessageBox>
#include <QTableWidget>
#include <QLabel>
#include <QLineEdit>
#include <QMenu>
#include <QFile>
#include "film.h"

class FilmWidget : public QWidget
{
    Q_OBJECT
private:
    film_list *films;

    QPushButton *add;
    QPushButton *search;
    QPushButton *del;
    QPushButton *trunc;
    QPushButton *tofile;
    QPushButton *fromfile;
    QTableWidget *table;

    QWidget *addw;
    QLineEdit *name;
    QLineEdit *length;
    QLineEdit *country;
    QPushButton *addok = new QPushButton("Ок");

    QWidget *delw;
    QLineEdit *delid;
    QPushButton *del_ok = new QPushButton("Ok");

    QWidget *searchw;
    QLineEdit *search_str;
    QPushButton *search_ok = new QPushButton("Search");
    QPushButton *search_cancel = new QPushButton("Cancel");

    QWidget *loadto;
    QLineEdit *to_path;
    QPushButton *to_ok = new QPushButton("Загрузить в файл");
    QPushButton *to_cancel = new QPushButton("Отмена");

    QWidget *loadfrom;
    QLineEdit *from_path;
    QPushButton *from_ok = new QPushButton("Загрузить из файла");

    QWidget *show_s_rec;
    film* reco;
    QLineEdit *nme;
    QLineEdit *leng;
    QLineEdit *cou;

    QPushButton *ser_del = new QPushButton("Delete");
    QPushButton *ser_ok = new QPushButton("Ok");



public:
    explicit FilmWidget(QWidget *parent = 0);
    ~FilmWidget();
private slots:
    void add_r();
    void send_add();
    void search_r();
    void del_r();
    void send_del();
    void send_search();
    void to_file();
    void from_file();
    void send_from();
    void send_to();
    void change_cell(int, int);
    void trun();
    void add_row(QString, int, QString);

    void sh_ser_rec(film*);
    void rec_changer();
    void sh_dl();
signals:
    void adder(QString, int, QString);
    void searcher(QString);
    void deleter(int);
    void to_filer(QString);
    void from_filer(QString);
    void ch_ce(int, int, QString);
    void truncate();
public slots:
};

#endif // FILMWIDGET_H
