#ifndef RECORD_H
#define RECORD_H

#include <QObject>

class record : public QObject
{
    Q_OBJECT
protected:
    int id;


public:
    explicit record(QObject *parent = 0);
    ~record();
    void set_id(int);
    int get_id();
signals:

public slots:
};

#endif // RECORD_H
